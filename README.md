<h1 align="center">Cypher Bot</h1>

<p align="center">O robô Cypher foi desenvolvido com o intuito ofensivo de scanear adversários e focá-los para o ataque, juntamente com a função de desvio de projéteis para maior durabilidade e eficiência dentro de uma batalha. O ponto-chave mais forte de Cypher é o seu caráter ofensivo, tornando-o bem expressivo em meio à luta, porém ao mesmo tempo se torna vulnerável a todos os seus adversários, tanto por exposição quanto por esgotamento de projétil.</p>

<p align="center">A confecção do robô foi singular, uma atividade nunca antes feita por mim, e muito gratificante desde o primeiro contato com esse desafio até o resultado alcançado.</p>