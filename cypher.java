package cypher;

import robocode.*;
import java.awt.*;

public class Cypher extends AdvancedRobot{
int direction = 1;

	public void run() {
	
		//COLORS
		setBodyColor(new Color(255,255,255));
		setGunColor(new Color(184,115,51));
		setRadarColor(new Color(0,127,255));
		setScanColor(Color.white);
		setBulletColor(new Color(0,255,255));
		
		//ADJUSTS
		setAdjustGunForRobotTurn(true); 
		turnRadarRightRadians(Double.POSITIVE_INFINITY);
		setAdjustRadarForRobotTurn(true);
	}


	public void onScannedRobot(ScannedRobotEvent e) {

	    double absBearing=e.getBearingRadians()+getHeadingRadians();
		double latVel=e.getVelocity() * Math.sin(e.getHeadingRadians() -absBearing);
		double gunTurnAmt;

		setTurnRadarLeftRadians(getRadarTurnRemainingRadians());

		if(Math.random()>.9){
			setMaxVelocity((12*Math.random())+12);
		}
        
		if (e.getDistance() > 150) {
			gunTurnAmt = robocode.util.Utils.normalRelativeAngle(absBearing- getGunHeadingRadians()+latVel/22);
			setTurnGunRightRadians(gunTurnAmt);
			setTurnRightRadians(robocode.util.Utils.normalRelativeAngle(absBearing-getHeadingRadians()+latVel/getVelocity()));
			setAhead((e.getDistance() - 140)*direction);
			setFire(3);
		}
		
		else{
			gunTurnAmt = robocode.util.Utils.normalRelativeAngle(absBearing- getGunHeadingRadians()+latVel/15);
			setTurnGunRightRadians(gunTurnAmt);
			setTurnLeft(-90-e.getBearing()); 
			setAhead((e.getDistance() - 140)*direction);
			setFire(3);
		}	
	}

	//AVOID WALLS
	public void onHitWall(HitWallEvent e){
		direction=-direction;
	}
	
	//WIN DANCE
	public void onWin(WinEvent e) {
		for (int i = 0; i < 50; i++) {
			turnRight(30);
			turnLeft(30);
		}
	}
	

}

